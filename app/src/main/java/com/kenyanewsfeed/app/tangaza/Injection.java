package com.kenyanewsfeed.app.tangaza;

import android.content.Context;
import android.support.annotation.NonNull;

import com.kenyanewsfeed.app.tangaza.data.posts.source.PostsRepository;
import com.kenyanewsfeed.app.tangaza.data.posts.source.local.PostsLocalDataSource;
import com.kenyanewsfeed.app.tangaza.data.posts.source.remote.PostsRemoteDataSource;

/**
 * Created by james on 9/21/2016.
 * Enable injection of mock implementations
 * for PostsDataSource at compile time. This is useful for testing, since it allows
 * us to use a fake instance of the class  to isolate the dependencies and run a test
 * hermetically
 */
public class Injection {

    public static PostsRepository providePostsRepository(@NonNull Context context){
        return PostsRepository.getInstance(PostsRemoteDataSource.getInstance(context),
                PostsLocalDataSource.getInstance(context));
    }

}

package com.kenyanewsfeed.app.tangaza;

import android.net.Uri;
import android.text.Html;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by james on 4/1/16.
 */
public class PostsFetcher {

//    private static final String TAG = "PostsFetcher";
//    public static int page = 1;
//    private List<PostItem> mPostItems = new ArrayList<>();
//
//
//    public byte[] getUrlBytes(String urlSpec) throws IOException{
//        URL url = new URL(urlSpec);
//
//        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
//
//
//        try{
//            ByteArrayOutputStream out = new ByteArrayOutputStream();
//            InputStream in = connection.getInputStream();
//
//            if(connection.getResponseCode() != HttpURLConnection.HTTP_OK){
//                throw new IOException(connection.getResponseMessage() + ": with " + urlSpec);
//            }
//
//            int bytesRead = 0;
//            byte [] buffer = new byte[1024];
//            while(( bytesRead = in.read(buffer)) > 0){
//                out.write(buffer, 0, bytesRead);
//            }
//            out.close();
//            return out.toByteArray();
//        } finally {
//            connection.disconnect();
//        }
//    }
//
//    public String getUrlString(String urlSpec) throws IOException{
//        return new String(getUrlBytes(urlSpec));
//    }
//
//    public List<PostItem> fetchPosts(){
//
//        List<PostItem> posts = new ArrayList<>();
//
//        try{
//            String url = Uri.parse("http://13.93.149.159/posts/")
//                    .buildUpon()
//                    .appendPath(String.valueOf(page))
//                    .build()
//                    .toString();
//
//            String jsonString = getUrlString(url);
//            Log.i(TAG, "Received JSON: " + jsonString);
//            JSONArray postsJsonArray = new JSONArray(jsonString);
//            mPostItems.addAll(parsePosts(posts, postsJsonArray));
//            Log.i(TAG, "listSize " + mPostItems.size());
//        } catch (IOException e)
//        {
//            Log.e(TAG, "Failed to fetch items: ", e);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        return mPostItems;
//    }
//
//    private List<PostItem> parsePosts(List<PostItem> posts, JSONArray postsJsonArray)
//        throws IOException, JSONException{
//        for(int i = 0; i < postsJsonArray.length(); i++){
//            JSONObject postJsonObject = postsJsonArray.getJSONObject(i);
//
//            PostItem item = new PostItem();
//            item.setId(postJsonObject.getString("id"));
//            item.setContent(stripHtml(postJsonObject.getString("content")));
//            item.setImage(postJsonObject.getString("img"));
//            item.setTitle(stripHtml(postJsonObject.getString("title")));
//            item.setLink(postJsonObject.getString("link"));
//            item.setBlog(postJsonObject.getString("blog"));
//
//            posts.add(item);
//        }
//
//        return posts;
//
//    }
//
//    public String stripHtml(String html) {
//        return Html.fromHtml(html).toString().trim();
//    }

}

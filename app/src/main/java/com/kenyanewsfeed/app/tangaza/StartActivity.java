package com.kenyanewsfeed.app.tangaza;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kenyanewsfeed.app.tangaza.posts.PostsActivity;


public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

//       ScheduleSyc.getInstance(new FirebaseJobDispatcher(new GooglePlayDriver(this))).run();

        Thread welcomeThread = new Thread() {
                @Override
                public void run() {
                    try {
                        super.run();
                        sleep(1000);  //Delay of 3 seconds
                    } catch (Exception e) {

                    } finally {
                        startActivity(PostsActivity.newIntent(StartActivity.this, "posts", R.id.all_stories));
                        finish();
                    }
                }
            };
            welcomeThread.start();
        }
}

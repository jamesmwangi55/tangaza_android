package com.kenyanewsfeed.app.tangaza.data.posts.source;

import android.support.annotation.NonNull;


import com.kenyanewsfeed.app.tangaza.models.Post;

import java.util.List;

/**
 * Created by james on 9/20/2016.
 */
public interface PostsDataSource {

    interface LoadPostsCallBack {
        void onLocalPostsLoaded(List<Post>  posts);
        void onRemotePostsLoaded(List<Post> postList);
        void onDataNotAvailable();
    }

    interface GetPostCallBack{
        void onLocalPostLoaded(Post post);
        void onRemotePostLoaded(Post post);
        void onDataNotAvailable();
    }


    void getLocalPosts(@NonNull LoadPostsCallBack callBack);
    void getLocalPost(@NonNull String id, @NonNull GetPostCallBack callBack);
    void getRemotePosts(@NonNull LoadPostsCallBack callBack);

    void savePost(@NonNull Post post);
    void refreshPosts(List<Post> postList);
    void deleteAllPosts();

}

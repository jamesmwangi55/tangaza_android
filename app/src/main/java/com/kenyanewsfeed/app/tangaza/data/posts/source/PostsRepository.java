package com.kenyanewsfeed.app.tangaza.data.posts.source;

import android.support.annotation.NonNull;

import com.kenyanewsfeed.app.tangaza.models.Post;

import java.util.List;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

/**
 * Created by james on 9/20/2016.
 */
public class PostsRepository implements PostsDataSource{
    
    private static PostsRepository INSTANCE = null;
    
    private final PostsDataSource mPostsRemoteDataSource;
    private final PostsDataSource mPostsLocalDataSource;

    // prevent direct instantiation
    private  PostsRepository(@NonNull PostsDataSource postsRemoteDataSource,
                            @NonNull PostsDataSource postsLocalDataSource){
        mPostsRemoteDataSource = checkNotNull(postsRemoteDataSource);
        mPostsLocalDataSource = checkNotNull(postsLocalDataSource);
    }

    // Returns a single instance of this class creating it if necessary
    public static PostsRepository getInstance(PostsDataSource postsRemoteDataSource,
                                              PostsDataSource postsLocalDataSource){
        if(INSTANCE == null){
            INSTANCE = new PostsRepository(postsRemoteDataSource, postsLocalDataSource);
        }
        return INSTANCE;
    }

    //used to force getInstance(PostsDataSource postsRemoteDataSource,PostsDataSource postsLocalDataSource)
    //to create new instance next time it's called
    public static void destroyInstance(){
            INSTANCE = null;
    }


    
    // get data from local data source
    // or remote data source, whichever is available first
    @Override
    public void getLocalPosts(@NonNull final LoadPostsCallBack callBack) {
        // Query the local storage if available
        // if not available query the network
        mPostsLocalDataSource.getLocalPosts( new LoadPostsCallBack() {
            @Override
            public void onLocalPostsLoaded(List<Post> posts) {
                callBack.onLocalPostsLoaded(posts);
            }

            @Override
            public void onRemotePostsLoaded(List<Post> postList) {
                callBack.onRemotePostsLoaded(postList);
            }

            @Override
            public void onDataNotAvailable() {
                getPostsFromRemoteDataSource(callBack);
            }
        });


        
    }



    private void getPostsFromRemoteDataSource(final LoadPostsCallBack callBack){
        mPostsRemoteDataSource.getRemotePosts(new LoadPostsCallBack() {
            @Override
            public void onLocalPostsLoaded(List<Post> posts) {
                callBack.onLocalPostsLoaded(posts);
            }

            @Override
            public void onRemotePostsLoaded(List<Post> postList) {
                callBack.onRemotePostsLoaded(postList);
                refreshLocalDataSource(postList);
            }

            @Override
            public void onDataNotAvailable() {
                callBack.onDataNotAvailable();
            }
        });
    }

    private void refreshLocalDataSource(List<Post> posts){
        mPostsLocalDataSource.deleteAllPosts();
        for(Post post: posts){
            mPostsLocalDataSource.savePost(post);
        }
    }

    @Override
    public void getLocalPost(@NonNull String id, @NonNull final GetPostCallBack callBack) {

    }

    @Override
    public void getRemotePosts( @NonNull final LoadPostsCallBack callBack) {
        mPostsRemoteDataSource.getRemotePosts(new LoadPostsCallBack() {
            @Override
            public void onLocalPostsLoaded(List<Post> posts) {
                callBack.onLocalPostsLoaded(posts);
            }

            @Override
            public void onRemotePostsLoaded(List<Post> postList) {
                callBack.onRemotePostsLoaded(postList);
                refreshLocalDataSource(postList);
            }

            @Override
            public void onDataNotAvailable() {
                callBack.onDataNotAvailable();
            }
        });
    }

    @Override
    public void savePost(@NonNull Post post) {

    }

    @Override
    public void refreshPosts(List<Post> postList) {
        refreshLocalDataSource(postList);
    }

    @Override
    public void deleteAllPosts() {

    }
}

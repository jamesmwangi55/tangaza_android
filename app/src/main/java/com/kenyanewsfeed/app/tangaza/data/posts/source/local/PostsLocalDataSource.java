package com.kenyanewsfeed.app.tangaza.data.posts.source.local;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.kenyanewsfeed.app.tangaza.data.posts.source.PostsDataSource;
import com.kenyanewsfeed.app.tangaza.models.Post;

import java.util.List;

import io.realm.RealmResults;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

/**
 * Created by james on 9/20/2016.
 */
public class PostsLocalDataSource implements PostsDataSource {

    private static final String TAG = PostsLocalDataSource.class.getSimpleName();

    private static PostsLocalDataSource INSTANCE;

    private PostsRealmController mPostsRealmController;


    // prevent direct instantiation
    private PostsLocalDataSource(@NonNull Context context){
        checkNotNull(context);
        mPostsRealmController = new PostsRealmController(context);
    }

    public static PostsLocalDataSource getInstance(@NonNull Context context){
        if(INSTANCE == null){
            INSTANCE = new PostsLocalDataSource(context);
        }
        return INSTANCE;
    }

    @Override
    public void getLocalPosts(@NonNull LoadPostsCallBack callBack) {
        RealmResults<Post> postLocalRealmResults = mPostsRealmController.getPosts();
        if(postLocalRealmResults.isEmpty()){
            Log.i(TAG, "getLocalPosts: " + "postLocalRealmResults is empty");
            callBack.onDataNotAvailable();
        } else {
            callBack.onLocalPostsLoaded(postLocalRealmResults);
        }
    }

    @Override
    public void getLocalPost(@NonNull String id, @NonNull GetPostCallBack callBack) {

    }

    @Override
    public void getRemotePosts( @NonNull LoadPostsCallBack callBack) {

    }

    @Override
    public void savePost(@NonNull Post post) {
        mPostsRealmController.savePost(post);
    }

    @Override
    public void refreshPosts(List<Post> postList) {

    }

    @Override
    public void deleteAllPosts() {
        mPostsRealmController.clearAll();
    }
}

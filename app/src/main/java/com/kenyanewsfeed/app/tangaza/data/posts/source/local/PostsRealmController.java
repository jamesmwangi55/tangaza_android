package com.kenyanewsfeed.app.tangaza.data.posts.source.local;

import android.app.Activity;
import android.app.Application;
import android.app.Fragment;
import android.content.Context;
import android.util.Log;

import com.kenyanewsfeed.app.tangaza.models.Post;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by james on 9/20/2016.
 */

public class PostsRealmController {

    private static final String TAG = PostsRealmController.class.getSimpleName();

    private static  PostsRealmController instance;
    private final Realm mRealm;

    

    public PostsRealmController(Context application){
        mRealm = Realm.getDefaultInstance();
    }

    public static PostsRealmController with (Fragment fragment){
        if(instance == null){
            instance = new PostsRealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static PostsRealmController with(Activity activity){
        if(instance == null){
            instance = new PostsRealmController(activity.getApplication());
        }
        return instance;
    }

    public static PostsRealmController with(Application application){
        if(instance == null){
            instance = new PostsRealmController(application);
        }
        return instance;
    }

    public static PostsRealmController getInstance(){
        return instance;
    }

    public Realm getRealm(){
        return mRealm;
    }

    //Refresh Realm Instance
    public void refresh(){
        //mRealm.isAutoRefresh()
    }

    // clear all objects from Post.class
    public void clearAll(){
        mRealm.beginTransaction();
        mRealm.delete(Post.class);
        mRealm.commitTransaction();
    }

    // find all objects in Post.class
    public RealmResults<Post> getPosts(){
        return mRealm.where(Post.class).findAll();
    }

    // query a single item with the given id
    public Post getPost(String id){
        return mRealm.where(Post.class).equalTo("id", id).findFirst();
    }

    // check if Post.class is Empty
    public boolean hasBooks(){
        return !mRealm.where(Post.class).findAll().isEmpty();
    }


    // save a Post.class object
    public void savePost(final Post post){
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Post postLocal = realm.createObject(Post.class);
                postLocal.setBlog(post.getBlog());
                postLocal.setContent(post.getContent());
                postLocal.setDownvotes(post.getDownvotes());
                postLocal.setFullContent(post.getFullContent());
                postLocal.setId(post.getId());
                postLocal.setImg(post.getImg());
                postLocal.setPublished(post.getPublished());
                postLocal.setLink(post.getLink());
                postLocal.setSource(post.getSource());
                postLocal.setViews(post.getViews());
                postLocal.setUpvotes(post.getUpvotes());
                postLocal.setTitle(post.getTitle());
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + post.getTitle() + " saved");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.i(TAG, "onError: " + error.getMessage());
            }
        });
    }
}

package com.kenyanewsfeed.app.tangaza.data.posts.source.remote;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.kenyanewsfeed.app.tangaza.data.posts.source.PostsDataSource;
import com.kenyanewsfeed.app.tangaza.models.Post;
import com.kenyanewsfeed.app.tangaza.services.PostsEndpoint;
import com.kenyanewsfeed.app.tangaza.utils.RetrofitBuilder;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by james on 9/20/2016.
 */
public class PostsRemoteDataSource implements PostsDataSource {

    private static final String TAG = PostsRemoteDataSource.class.getSimpleName();


    private static PostsRemoteDataSource INSTANCE;
    private Retrofit mRetrofit;
    private RetrofitBuilder mRetrofitBuilder;
    private Context mContext;

    private PostsRemoteDataSource(Context context){
        mContext = context;
        mRetrofitBuilder = new RetrofitBuilder(mContext);
        mRetrofit = mRetrofitBuilder.getRetrofit();
    }

    public static PostsRemoteDataSource getInstance(Context context){
        if(INSTANCE == null){
            INSTANCE = new PostsRemoteDataSource(context);
        }
        return INSTANCE;
    }




    @Override
    public void getLocalPosts(@NonNull LoadPostsCallBack callBack) {

    }

    @Override
    public void getLocalPost(@NonNull String id, @NonNull GetPostCallBack callBack) {

    }

    @Override
    public void getRemotePosts(@NonNull final LoadPostsCallBack callBack) {
        PostsEndpoint postsEndpoint = mRetrofit.create(PostsEndpoint.class);
        Call<List<Post>> call = postsEndpoint.getPosts();
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if(response.isSuccessful()){
                    callBack.onRemotePostsLoaded(response.body());
                } else {
                    Log.i(TAG, "onResponse: Failed With Error: " + Integer.toString(response.code()));
                }
            }
            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Log.i(TAG, "onFailure: " + t.getMessage());
                callBack.onDataNotAvailable();
            }
        });
    }

    @Override
    public void savePost(@NonNull Post post) {

    }

    @Override
    public void refreshPosts(List<Post> postList) {

    }


    @Override
    public void deleteAllPosts() {

    }
}

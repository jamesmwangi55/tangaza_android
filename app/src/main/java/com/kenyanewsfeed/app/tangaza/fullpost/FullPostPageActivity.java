package com.kenyanewsfeed.app.tangaza.fullpost;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.view.Menu;

import com.kenyanewsfeed.app.tangaza.R;
import com.kenyanewsfeed.app.tangaza.SingleFragmentActivity;

/**
 * Created by james on 4/5/16.
 */
public class FullPostPageActivity extends SingleFragmentActivity {

    private static final String POST_URI = "post_uri";
    private NavigationView mNavigationView;
    protected int mCheckedMenuItem;

    public static Intent newIntent(Context context, String postUri){
        Intent intent = new Intent(context, FullPostPageActivity.class);
        intent.putExtra(POST_URI, postUri);
        return intent;
    }

    @Override
    protected Fragment createFragment() {
        return new FullPostPageFragment().newInstance(getIntent().getStringExtra(POST_URI));
    }

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        mNavigationView.getMenu().clear();
        mNavigationView.inflateMenu(R.menu.drawer);
        mNavigationView.getMenu().findItem(R.id.all_stories);
        mNavigationView.getMenu().findItem(R.id.nairobi_wire);
        mNavigationView.getMenu().findItem(R.id.nation);
        mNavigationView.getMenu().findItem(R.id.standard);
        mNavigationView.getMenu().findItem(R.id.football);
        mNavigationView.getMenu().findItem(R.id.showbiz);
        mNavigationView.setCheckedItem(mCheckedMenuItem);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }


}

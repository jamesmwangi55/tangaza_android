package com.kenyanewsfeed.app.tangaza.fullpost;

import com.kenyanewsfeed.app.tangaza.R;
import com.kenyanewsfeed.app.tangaza.utils.CheckNetWork;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

/**
 * Created by james on 4/5/16.
 */
public class FullPostPageFragment extends Fragment {
    private static final String ARG_URI = "photo_page_url";

    private String mUri;
    private WebView mWebView;
    private ProgressBar mProgressBar;


    public static FullPostPageFragment newInstance(String url){
        Bundle args = new Bundle();
        args.putString(ARG_URI, url);

        FullPostPageFragment fragment = new FullPostPageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!CheckNetWork.isNetworkAvailableAndConnected(getActivity())) {
            Toast.makeText(getActivity(), "Your Device is Not Connected To the Internet", Toast.LENGTH_SHORT).show();
            return;
        }
        mUri = getArguments().getString(ARG_URI);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_full_post_page, container, false);

        mProgressBar = (ProgressBar) view.findViewById(R.id.fragment_full_post_page_progress_bar);
        mProgressBar.setMax(100);

        mWebView = (WebView) view.findViewById(R.id.full_page_web_view);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebChromeClient(new WebChromeClient(){
            public void onProgressChanged(WebView webView, int newProgress){
                if(newProgress == 100){
                    mProgressBar.setVisibility(View.GONE);
                } else {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mProgressBar.setProgress(newProgress);
                }


            }

            public void onReceivedTitle(WebView webView, String title){
                AppCompatActivity activity = (AppCompatActivity) getActivity();
                if(activity!=null){
                    activity.getSupportActionBar().setSubtitle(title);
                }
            }

        });

        mWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });

        mWebView.loadUrl(mUri);
        return view;
    }

}

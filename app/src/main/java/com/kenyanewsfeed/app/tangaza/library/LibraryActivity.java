package com.kenyanewsfeed.app.tangaza.library;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import com.kenyanewsfeed.app.tangaza.R;
import com.kenyanewsfeed.app.tangaza.SingleFragmentActivity;

public class LibraryActivity extends SingleFragmentActivity {

   public static Intent newIntent(Context context){
       Intent intent = new Intent(context, LibraryActivity.class);

       return intent;
   }

    @Override
    protected Fragment createFragment() {
        return new LibraryFragment().newInstance();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_library;
    }
}

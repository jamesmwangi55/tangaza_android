package com.kenyanewsfeed.app.tangaza.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.Html;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

@Generated("org.jsonschema2pojo")
public class Nation extends RealmObject implements Parcelable {

    @SerializedName("blog")
    @Expose
    private String blog;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("downvotes")
    @Expose
    private Integer downvotes;
    @SerializedName("fullContent")
    @Expose
    private String fullContent;
    @PrimaryKey
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("img")
    @Expose
    private String img;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("published")
    @Expose
    private String published;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("upvotes")
    @Expose
    private Integer upvotes;
    @SerializedName("views")
    @Expose
    private Integer views;

    public Nation(){

    }

    private Nation(Parcel in) {
        blog = in.readString();
        content = in.readString();
        downvotes = in.readInt();
        fullContent = in.readString();
        id = in.readString();
        img = in.readString();
        link = in.readString();
        published = in.readString();
        source = in.readString();
        title = in.readString();
        upvotes = in.readInt();
        views = in.readInt();
    }

    /**
     *
     * @return
     * The blog
     */
    public String getBlog() {
        return blog;
    }

    /**
     *
     * @param blog
     * The blog
     */
    public void setBlog(String blog) {
        this.blog = blog;
    }

    /**
     *
     * @return
     * The content
     */
    public String getContent() {
        return stripHtml(content);
    }

    /**
     *
     * @param content
     * The content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     *
     * @return
     * The downvotes
     */
    public Integer getDownvotes() {
        return downvotes;
    }

    /**
     *
     * @param downvotes
     * The downvotes
     */
    public void setDownvotes(Integer downvotes) {
        this.downvotes = downvotes;
    }

    /**
     *
     * @return
     * The fullContent
     */
    public String getFullContent() {
        return fullContent;
    }

    /**
     *
     * @param fullContent
     * The fullContent
     */
    public void setFullContent(String fullContent) {
        this.fullContent = fullContent;
    }

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The img
     */
    public String getImg() {
        return img;
    }

    /**
     *
     * @param img
     * The img
     */
    public void setImg(String img) {
        this.img = img;
    }

    /**
     *
     * @return
     * The link
     */
    public String getLink() {
        return link;
    }

    /**
     *
     * @param link
     * The link
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     *
     * @return
     * The published
     */
    public String getPublished() {
        return published;
    }

    /**
     *
     * @param published
     * The published
     */
    public void setPublished(String published) {
        this.published = published;
    }

    /**
     *
     * @return
     * The source
     */
    public String getSource() {
        return source;
    }

    /**
     *
     * @param source
     * The source
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     *
     * @return
     * The title
     */
    public String getTitle() {
        return stripHtml(title);
    }

    /**
     *
     * @param title
     * The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The upvotes
     */
    public Integer getUpvotes() {
        return upvotes;
    }

    /**
     *
     * @param upvotes
     * The upvotes
     */
    public void setUpvotes(Integer upvotes) {
        this.upvotes = upvotes;
    }

    /**
     *
     * @return
     * The views
     */
    public Integer getViews() {
        return views;
    }

    /**
     *
     * @param views
     * The views
     */
    public void setViews(Integer views) {
        this.views = views;
    }

    @Override
    public String toString(){
        return stripHtml(title);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(blog);
        dest.writeString(content);
        dest.writeInt(downvotes);
        dest.writeString(fullContent);
        dest.writeString(id);
        dest.writeString(img);
        dest.writeString(link);
        dest.writeString(published);
        dest.writeString(source);
        dest.writeString(title);
        dest.writeInt(upvotes);
        dest.writeInt(views);
    }

    public static final Parcelable.Creator<Nation> CREATOR = new Parcelable.Creator<Nation>() {
        public Nation createFromParcel(Parcel in) {
            return new Nation(in);
        }

        public Nation[] newArray(int size) {
            return new Nation[size];
        }
    };

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString().trim();
    }
}
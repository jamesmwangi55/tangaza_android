package com.kenyanewsfeed.app.tangaza.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.Html;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by james on 9/20/2016.
 */

public class Post extends RealmObject {


    private String blog;

    private String content;

    private Integer downvotes;

    private String fullContent;

    @PrimaryKey
    private String id;

    private String img;

    private String link;

    private String published;

    private String source;

    private String title;

    private Integer upvotes;

    private Integer views;

    private String  article;


    public String getBlog() {
        return blog;
    }

    public void setBlog(String blog) {
        this.blog = blog;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getDownvotes() {
        return downvotes;
    }

    public void setDownvotes(Integer downvotes) {
        this.downvotes = downvotes;
    }

    public String getFullContent() {
        return Html.fromHtml(fullContent).toString().trim();
        //return fullContent;
    }

    public void setFullContent(String fullContent) {
        this.fullContent = fullContent;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTitle() {
        return stripHtml(title);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getUpvotes() {
        return upvotes;
    }

    public void setUpvotes(Integer upvotes) {
        this.upvotes = upvotes;
    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public String getArticle() {
        return Html.fromHtml(article).toString().trim();
    }

    public void setArticle(String article) {
        this.article = article;
    }

    @Override
    public String toString(){
        return stripHtml(title);
    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString().trim();
    }


}

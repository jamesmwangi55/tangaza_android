package com.kenyanewsfeed.app.tangaza.post;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.kenyanewsfeed.app.tangaza.R;
import com.kenyanewsfeed.app.tangaza.SingleFragmentActivity;

public class PostActivity extends SingleFragmentActivity {

    private static final String TAG = PostActivity.class.getSimpleName();
    private static final String EXTRA_POST = "extra_post";
    private static final String ARGS_POST = "args_post";

    private PostItem mPost;

    public static Intent newIntent(Context context, PostItem post){
        Intent intent = new Intent(context, PostActivity.class);
        intent.putExtra(EXTRA_POST, post);
        return intent;

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_post;
    }

    @Override
    protected Fragment createFragment() {
        mPost = (PostItem) getIntent().getSerializableExtra(EXTRA_POST);
        PostFragment postFragment = PostFragment.newInstance(mPost);

        getSupportActionBar().hide();

        return postFragment;
    }

    private void showHideActionBar(){
        getSupportActionBar().hide();
    }




    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(ARGS_POST, mPost);
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mPost = (PostItem) savedInstanceState.getSerializable(ARGS_POST);
    }


}

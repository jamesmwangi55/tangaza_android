package com.kenyanewsfeed.app.tangaza.post;

import com.kenyanewsfeed.app.tangaza.BasePresenter;
import com.kenyanewsfeed.app.tangaza.BaseView;
import com.kenyanewsfeed.app.tangaza.models.Post;
import com.kenyanewsfeed.app.tangaza.posts.PostsContract;

/**
 * Created by james on 10/5/2016.
 */

public interface PostContract {
    interface View extends BaseView<Presenter>{
        void showPost(Post postLocal);

    }

    interface Presenter extends BasePresenter{
        void fetchPost(String postId);
    }
}

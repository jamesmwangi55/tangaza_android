package com.kenyanewsfeed.app.tangaza.post;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kenyanewsfeed.app.tangaza.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostFragment extends Fragment {

    private static final String TAG = PostFragment.class.getSimpleName();

    private static final String ARG_POST = "post";
    private PostItem mPost;

    private TextView mTitleTextView;
    @BindView(R.id.post_logo) ImageView mLogoImageView;
    @BindView(R.id.article) TextView mArticleTextView;
    private ImageView mToolBarImage;
    private NestedScrollView mNestedScrollView;
    private AppBarLayout mAppBarLayout;

    private Unbinder mUnbinder;

    public static PostFragment newInstance(PostItem postLocal) {

        Bundle args = new Bundle();
        args.putSerializable(ARG_POST, postLocal);
        PostFragment fragment = new PostFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public PostFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPost = (PostItem) getArguments().getSerializable(ARG_POST);
        Log.i(TAG, "onCreate: " + mPost.getImg());
        Log.i(TAG, "onCreate: " + mPost.getBlog());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_post, container, false);
        mUnbinder = ButterKnife.bind(this, view);

        // TODO: 11/3/2016 start from here
        String blogNameArray[] = mPost.getBlog().split("\\.");

        int id = getActivity().getResources().getIdentifier(blogNameArray[0], "drawable", getActivity().getPackageName());
        if(blogNameArray[0].equalsIgnoreCase("niaje")){
            mLogoImageView.setBackgroundColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
        } else {
            mLogoImageView.setBackgroundColor(getActivity().getResources().getColor(R.color.cardview_light_background));
        }

        Picasso.with(getActivity())
                .load(id)
                .into(mLogoImageView);

        mTitleTextView = (TextView) view.findViewById(R.id.post_title);
        mTitleTextView.setText(mPost.getTitle());
        mArticleTextView.setText(mPost.getArticle());

        mAppBarLayout = (AppBarLayout) getActivity().findViewById(R.id.app_bar_layout);

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mToolBarImage = (ImageView) getActivity().findViewById(R.id.image_toolbar);

        if(mToolBarImage!=null){
            mToolBarImage.setAlpha(0.9f);
                Picasso.with(getActivity())
                        .load(mPost.getImg())
                        .placeholder(R.drawable.placeholder_image)
                        .into(mToolBarImage);
        } else {
            //Toast.makeText(getActivity(), "Null", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    public PostItem getPost(){
        return mPost;
    }

}

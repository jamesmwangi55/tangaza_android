package com.kenyanewsfeed.app.tangaza.posts;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.kenyanewsfeed.app.tangaza.Injection;
import com.kenyanewsfeed.app.tangaza.R;
import com.kenyanewsfeed.app.tangaza.library.LibraryFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostsActivity extends AppCompatActivity {
    private static final String TAG = PostsActivity.class.getSimpleName();
    String mTitle = "";
    String mCategory = "";
    protected int mCheckedMenuItem;

    private static final String EXTRA_CATEGORY =  "category";
    private static final String EXTRA_CHECKED =  "checkedMenuItem";
    private static final String STATE_CHECKED = "checkedMenuItem";

    @BindView(R.id.tabLayout) TabLayout mTabLayout;
    @BindView(R.id.pager) ViewPager mViewPager;
    @BindView(R.id.toolbar) Toolbar mToolbar;

    private PostsPresenter mPostsPresenter;
    PostsFragment mPostsFragment;

    public static Intent newIntent(Context context, String category, int checkedMenuItem){
        Intent intent = new Intent(context, PostsActivity.class);
        intent.putExtra(EXTRA_CATEGORY, category);
        intent.putExtra(EXTRA_CHECKED, checkedMenuItem);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posts);
        mCategory = getIntent().getExtras().getString(EXTRA_CATEGORY);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        mPostsFragment  = PostsFragment.newInstance(mCategory);

//        FragmentManager fm = getSupportFragmentManager();
//        Fragment fragment = fm.findFragmentById(R.id.pager);
//
//        if(fragment == null){
//            fragment = postsFragment;
//            fm.beginTransaction()
//                    .add(R.id.fragment_container, fragment)
//                    .commit();
//        }



        setUpViewPager(mViewPager);
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab)
            {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab)
            {

            }
        });
        mTabLayout.setupWithViewPager(mViewPager);
        mPostsPresenter = new PostsPresenter(
                Injection.providePostsRepository(getApplicationContext()),
                mPostsFragment
        );

    }

    //    @Override
//    protected int getLayoutResId() {
//        return R.layout.activity_posts;
//    }
//
//    @Override
//    protected Fragment createFragment() {
//        mCategory = getIntent().getExtras().getString(EXTRA_CATEGORY);
//        PostsFragment postsFragment = PostsFragment.newInstance(mCategory);
//
//        ButterKnife.bind(this);
//
//        // Create the presenter
//        mPostsPresenter = new PostsPresenter(
//                Injection.providePostsRepository(getApplicationContext()),
//                postsFragment
//        );
//
//        setUpViewPager(mViewPager);
//        mTabLayout.setupWithViewPager(mViewPager);
//
//        return postsFragment;
//    }

    private void setUpViewPager(ViewPager viewPager){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(mPostsFragment, "All");
        adapter.addFragment(LibraryFragment.newInstance(), "Library");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            // Create the presenter

        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title){
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return  mFragmentTitleList.get(position);
        }
    }



    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

}

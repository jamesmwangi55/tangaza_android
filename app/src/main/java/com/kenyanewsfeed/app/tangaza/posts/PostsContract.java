package com.kenyanewsfeed.app.tangaza.posts;

import android.support.annotation.NonNull;

import com.kenyanewsfeed.app.tangaza.BasePresenter;
import com.kenyanewsfeed.app.tangaza.BaseView;
import com.kenyanewsfeed.app.tangaza.models.Post;

import java.util.List;

/**
 * Created by james on 9/21/2016.
 */

public interface PostsContract {
    interface View extends BaseView<Presenter>{
        void setLoadingIndicator(boolean active);
        void showPosts(List<Post> postList);
        void showNoNetwork(boolean show);
        void showNoPosts();
        void refreshView();
        void notifyDataChanged();
    }
    interface Presenter extends BasePresenter{
        void openPostDetails(@NonNull Post requestedPost);
        void fetchRemotePosts(String category, int page);
        void fetchLocalPosts(String category, int page);
        void refreshLocalRepository();
    }
}

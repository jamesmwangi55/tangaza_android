package com.kenyanewsfeed.app.tangaza.posts;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.azoft.carousellayoutmanager.CarouselLayoutManager;
import com.kenyanewsfeed.app.tangaza.Application;
import com.kenyanewsfeed.app.tangaza.R;
import com.kenyanewsfeed.app.tangaza.models.Post;
import com.kenyanewsfeed.app.tangaza.post.PostActivity;
import com.kenyanewsfeed.app.tangaza.post.PostItem;
import com.kenyanewsfeed.app.tangaza.utils.ConnectivityReceiver;
import com.kenyanewsfeed.app.tangaza.utils.RecyclerClickListener;
import com.kenyanewsfeed.app.tangaza.utils.RecyclerTouchListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

/**
 * Created by james on 4/1/16.
 */
public class PostsFragment extends Fragment implements PostsContract.View, ConnectivityReceiver.ConnectivityReceiverListener{

    private PostsContract.Presenter mPresenter;

    private static final String TAG = "PostsFragment";
    private static final String SAVED_POST_ITEM_LIST = "subtitle";
    private static final String ARG_CATEGORY = PostsFragment.class.getSimpleName() + "category";
    private static final String ARG_CHECKED= PostsFragment.class.getSimpleName() + "checked";
    private static final String ARG_RESUMED = "FragmentResumed";

    private TextView mNetworkTextView;
    final CarouselLayoutManager mCarouselLayoutManager = new CarouselLayoutManager(CarouselLayoutManager.HORIZONTAL);

    private FragmentStatePagerAdapter mFragmentPagerAdapter;
    private RemotePostAdapter mRemotePostAdapter;

    private boolean isFragmentResumed = true;

    private Unbinder mUnbinder;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private ImageView mToolBarImage;

    private List<Post> mPosts = new ArrayList<>();
    String mCategory;
    protected int mCheckedMenuItem;

    public static PostsFragment newInstance(String category) {
        Bundle args = new Bundle();
        args.putString(ARG_CATEGORY, category);
        PostsFragment fragment = new PostsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mCategory = getArguments().getString(ARG_CATEGORY);
        mCheckedMenuItem = getArguments().getInt(ARG_CHECKED);
        Toast.makeText(getActivity(), mCategory, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onResume() {
        super.onResume();
        if(isFragmentResumed){
            if(mPresenter!=null){
                mPresenter.start();
            } else {
               Toast.makeText(getActivity(), "mPresenter is null", Toast.LENGTH_SHORT).show();
            }
            isFragmentResumed = false;
        }
        Application.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_posts, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        if (savedInstanceState != null) {
           // mItems = savedInstanceState.getSerArrayList(SAVED_POST_ITEM_LIST);
            isFragmentResumed = savedInstanceState.getBoolean(ARG_RESUMED);
        }
        mRecyclerView = (RecyclerView) view.findViewById(R.id.posts_recycler_view);
        mLayoutManager = new LinearLayoutManager(getContext());
       // mLayoutManager = new GridLayoutManager(getActivity(), 2);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        //mRecyclerView.setNestedScrollingEnabled(false);
        implementRecyclerViewClickListeners();

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mToolBarImage = (ImageView) getActivity().findViewById(R.id.image_toolbar);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
       // outState.putParcelableArrayList(SAVED_POST_ITEM_LIST, (ArrayList<? extends Parcelable>) mItems);
        outState.putBoolean(ARG_RESUMED, isFragmentResumed);
        super.onSaveInstanceState(outState);
    }



    @Override
    public void setLoadingIndicator(boolean active) {
    }


    @Override
    public void showPosts(List<Post> postList) {
        if(isAdded()){

            if(mToolBarImage != null){
                Picasso.with(getActivity())
                        .load(postList.get(new Random().nextInt(postList.size())).getImg())
                        .placeholder(R.drawable.placeholder_image)
                        .into(mToolBarImage);
            }

            if(mRemotePostAdapter == null){
                mRemotePostAdapter = new RemotePostAdapter(postList, getActivity());
                mRecyclerView.addItemDecoration(new GridItemDividerDecoration(getActivity(),
                        R.dimen.divider_height, R.color.divider));
                mRecyclerView.setAdapter(mRemotePostAdapter);
                mPosts = postList;
            } else {
                mRemotePostAdapter.setPosts(postList);
                mPosts = postList;
                mRemotePostAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void showNoNetwork(boolean show) {
        //mNetworkTextView.setVisibility(show ? View.VISIBLE : View.GONE);
    }


    @Override
    public void showNoPosts() {
        Toast.makeText(getActivity(), "No Posts", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void refreshView() {
        mPresenter.start();
    }

    @Override
    public void notifyDataChanged() {
        if(mRemotePostAdapter!= null){
            mRemotePostAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void setPresenter(@NonNull PostsContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showConnectionStatus(isConnected);
    }

    private void showConnectionStatus(boolean isConnected){
        if(isConnected){
            if(mPresenter!=null){
                mPresenter.start();
            }
            showNoNetwork(false);
        } else {
            showNoNetwork(true);
        }
    }

    private void implementRecyclerViewClickListeners(){
        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), mRecyclerView, new RecyclerClickListener() {
            @Override
            public void onClick(View view, int position) {

                if(mPosts!=null){
                    Post post = mPosts.get(position);

                    PostItem postItem = new PostItem();
                    postItem.setArticle(post.getArticle());
                    postItem.setBlog(post.getBlog());
                    postItem.setContent(post.getContent());
                    postItem.setDownvotes(post.getDownvotes());
                    postItem.setFullContent(post.getFullContent());
                    postItem.setId(post.getId());
                    postItem.setLink(post.getLink());
                    postItem.setImg(post.getImg());
                    postItem.setTitle(post.getTitle());
                    postItem.setUpvotes(post.getUpvotes());
                    postItem.setSource(post.getSource());
                    postItem.setViews(post.getViews());
                    postItem.setPublished(post.getPublished());

                    startActivity(PostActivity.newIntent(getActivity(), postItem));
                }
            }
            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

}
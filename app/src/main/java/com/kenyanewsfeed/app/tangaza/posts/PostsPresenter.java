package com.kenyanewsfeed.app.tangaza.posts;

import android.support.annotation.NonNull;
import android.util.Log;

import com.kenyanewsfeed.app.tangaza.data.posts.source.PostsDataSource;
import com.kenyanewsfeed.app.tangaza.data.posts.source.PostsRepository;
import com.kenyanewsfeed.app.tangaza.models.Post;

import java.util.List;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

/**
 * Created by james on 9/21/2016.
 */

public class PostsPresenter implements PostsContract.Presenter {

    private static final String TAG = PostsPresenter.class.getSimpleName();

    private final PostsRepository mPostsRepository;
    private final PostsContract.View mView;

    public PostsPresenter(@NonNull PostsRepository postsRepository, @NonNull PostsContract.View  view){
        mPostsRepository = checkNotNull(postsRepository);
        mView = checkNotNull(view);
        mView.setPresenter(this);
    }

    @Override
    public void openPostDetails(@NonNull Post requestedPost) {

    }

    @Override
    public void fetchRemotePosts(String category, int page) {

    }

    @Override
    public void fetchLocalPosts(String category, int page) {
        mPostsRepository.getLocalPosts(new PostsDataSource.LoadPostsCallBack() {
            @Override
            public void onLocalPostsLoaded(List<Post> postLocalList) {
                mView.setLoadingIndicator(false);
                mView.showPosts(postLocalList);
                mView.notifyDataChanged();
                if(!postLocalList.isEmpty()){
                    fetchRemoteRepository();
                }
            }

            @Override
            public void onRemotePostsLoaded(List<Post> postList) {
                mView.setLoadingIndicator(false);
                mView.showPosts(postList);
                mView.notifyDataChanged();
            }

            @Override
            public void onDataNotAvailable() {
                mView.setLoadingIndicator(false);
                Log.i(TAG, "onDataNotAvailable: "+ "Not data Retrieved");
            }
        });
    }

    private void fetchRemoteRepository(){
        mView.setLoadingIndicator(true);
        mPostsRepository.getRemotePosts(new PostsDataSource.LoadPostsCallBack() {
            @Override
            public void onLocalPostsLoaded(List<Post> postLocalList) {
                mView.setLoadingIndicator(false);
                Log.i(TAG, "onLocalPostsLoaded: Local Posts Loaded Again");
            }

            @Override
            public void onRemotePostsLoaded(List<Post> postList) {
                mView.setLoadingIndicator(false);
                mView.showPosts(postList);
            }

            @Override
            public void onDataNotAvailable() {
                mView.setLoadingIndicator(false);
            }
        });
    }

    @Override
    public void refreshLocalRepository() {

    }

    @Override
    public void start() {
        mView.setLoadingIndicator(true);
        loadPosts();
    }

    private void loadPosts(){
        fetchLocalPosts("posts", 1);
    }

}

package com.kenyanewsfeed.app.tangaza.posts;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.kenyanewsfeed.app.tangaza.R;
import com.kenyanewsfeed.app.tangaza.models.Post;

import java.util.List;

/**
 * Created by james on 10/29/2016.
 */

public class RemotePostAdapter extends RecyclerView.Adapter<RemotePostHolder> {

    private static final String TAG =RemotePostAdapter.class.getSimpleName();

    List<Post> mPosts;
    private Context mContext;
    private int mViewWidth;
    private int mViewHeight;

    public RemotePostAdapter(List<Post> posts, Context context){
        mPosts = posts;
        mContext = context;
    }

    @Override
    public RemotePostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        final View view = layoutInflater.inflate(R.layout.post_item, parent, false);

        ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        if(viewTreeObserver.isAlive()){
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    mViewWidth = view.getWidth();
                    mViewHeight = view.getHeight();
                }
            });

        }

        return new RemotePostHolder(view);
    }

    @Override
    public void onBindViewHolder(RemotePostHolder holder, int position) {
        Post post = mPosts.get(position);
        holder.bindPost(post, mContext,mViewWidth, mViewHeight);
        Log.i(TAG, "onBindViewHolder: " + "Height: " + mViewHeight + "Width: " + mViewWidth);
    }





    @Override
    public int getItemCount() {
        return mPosts.size();
    }

    public void setPosts(List<Post> posts){
        mPosts = posts;
    }
}

package com.kenyanewsfeed.app.tangaza.posts;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kenyanewsfeed.app.tangaza.R;
import com.kenyanewsfeed.app.tangaza.models.Post;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by james on 10/29/2016.
 */

public class RemotePostHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private static final String TAG = RemotePostHolder.class.getSimpleName();

    @BindView(R.id.post_item_image_view) ImageView mPostImage;
    @BindView(R.id.post_item_title_text_view) TextView mPostTitleTextView;
    @BindView(R.id.post_item_logo_image_view) ImageView mPostLogoImageView;
    private Post mPost;

    public RemotePostHolder(View itemView) {
        super(itemView);
        mPost = new Post();
        ButterKnife.bind(this, itemView);
//        Log.i(TAG, "RemotePostHolder: " + Integer.toString(itemView.getHeight()));
        //todo start from here, determine item height and width
    }

    public void bindPost(Post post, Context context, int width, int height){
        mPostTitleTextView.setText(post.getTitle());
        Log.i(TAG, "bindPost: " + width + " : " + height);
        Picasso.with(context)
                .load(post.getImg())
                .placeholder(R.drawable.placeholder_image)
                .resize(600, 400)
                .onlyScaleDown()
                .into(mPostImage);
        mPostImage.setScaleType(ImageView.ScaleType.FIT_XY);
        mPostImage.setAdjustViewBounds(true);

//        String blogNameArray[] = post.getBlog().split("\\.");
//
//        int id = context.getResources().getIdentifier(blogNameArray[0], "drawable", context.getPackageName());
//        if(blogNameArray[0].equalsIgnoreCase("niaje")){
//            mPostLogoImageView.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
//        } else {
//            mPostLogoImageView.setBackgroundColor(context.getResources().getColor(R.color.cardview_light_background));
//        }
//
//        Picasso.with(context)
//                .load(id)
//                .into(mPostLogoImageView);

    }

    @Override
    public void onClick(View v) {
        Snackbar.make(v, "Clicked", Snackbar.LENGTH_LONG).show();
    }
}

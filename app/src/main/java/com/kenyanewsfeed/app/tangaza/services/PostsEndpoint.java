package com.kenyanewsfeed.app.tangaza.services;

import com.kenyanewsfeed.app.tangaza.models.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by DI on 7/20/2016.
 */
public interface PostsEndpoint {
    @GET("posts")
    Call<List<Post>> getPosts();
}

package com.kenyanewsfeed.app.tangaza.sync;

import android.content.Context;
import android.util.Log;

import com.kenyanewsfeed.app.tangaza.Injection;
import com.kenyanewsfeed.app.tangaza.data.posts.source.PostsDataSource;
import com.kenyanewsfeed.app.tangaza.data.posts.source.PostsRepository;
import com.kenyanewsfeed.app.tangaza.models.Post;

import java.util.List;

/**
 * Created by james on 9/21/2016.
 */

public class PostsToSync {
    private static final String TAG = PostsToSync.class.getSimpleName();
    private final PostsRepository mPostsRepository;
    private final Context mContext;

    public PostsToSync(Context context){
        mContext = context;
        mPostsRepository = Injection.providePostsRepository(context);
    }

    public  void syncPosts(){
        mPostsRepository.getRemotePosts(new PostsDataSource.LoadPostsCallBack() {
            @Override
            public void onLocalPostsLoaded(List<Post> postLocalList) {

            }

            @Override
            public void onRemotePostsLoaded(List<Post> postList) {
                mPostsRepository.refreshPosts(postList);
            }

            @Override
            public void onDataNotAvailable() {
                Log.i(TAG, "onDataNotAvailable: " + "No posts retrieved");
            }
        });
    }
}

package com.kenyanewsfeed.app.tangaza.sync;

import android.content.SyncContext;
import android.util.Log;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;

/**
 * Created by james on 9/21/2016.
 */

public class ScheduleSyc implements Runnable {

    private static final String TAG = ScheduleSyc.class.getSimpleName();

    private static ScheduleSyc INSTANCE = null;


    private final FirebaseJobDispatcher mFirebaseJobDispatcher;
    private final Job.Builder mBuilder;

    public static ScheduleSyc getInstance(FirebaseJobDispatcher firebaseJobDispatcher){
        if(INSTANCE == null){
            INSTANCE = new ScheduleSyc(firebaseJobDispatcher);
        }
        return INSTANCE;
    }


    public ScheduleSyc(FirebaseJobDispatcher dispatcher){
        mFirebaseJobDispatcher = dispatcher;

        if(mFirebaseJobDispatcher != null){
            Log.i(TAG, "ScheduleSyc: " + "Not Null Dispatcher");
        }

        mBuilder = dispatcher
                .newJobBuilder()
                .setService(SyncJobService.class)
                .setTag("Sync Posts")
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                .setTrigger(Trigger.NOW)
                .setLifetime(Lifetime.UNTIL_NEXT_BOOT)
                .setRecurring(false);

    }

    @Override
    public void run() {

        mFirebaseJobDispatcher.mustSchedule(mBuilder.build());
    }

}

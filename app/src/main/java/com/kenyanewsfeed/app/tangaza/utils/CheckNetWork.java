package com.kenyanewsfeed.app.tangaza.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.widget.Toast;

/**
 * Created by james on 4/8/16.
 */
public class CheckNetWork  {

    public static boolean isNetworkAvailableAndConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean isNetworkAvailable = cm.getActiveNetworkInfo() != null;
        boolean isNetworkConnected = isNetworkAvailable &&
                cm.getActiveNetworkInfo().isConnected();

        return isNetworkConnected;
    }


    public static void noNetworkToast (Context context){
        Toast.makeText(context,
                "No internet connection, Please confirm you have an internet connection before proceeding",
                Toast.LENGTH_LONG).show();

    }
}

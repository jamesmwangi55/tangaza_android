package com.kenyanewsfeed.app.tangaza.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.kenyanewsfeed.app.tangaza.library.LibraryFragment;
import com.kenyanewsfeed.app.tangaza.posts.PostsFragment;

/**
 * Created by james on 12/7/2016.
 */

public class Pager extends FragmentStatePagerAdapter {

    private static final String TAG = Pager.class.getSimpleName();

    //integer to count number of tabs
    int tabCount;

    //Constructor for the class
    public Pager(FragmentManager fm, int tabCount) {
        super(fm);
        //Initialize tab count
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        Log.i(TAG, "getItem: getItem");
        //Returning the current tabs

        switch (position) {
            case 0:
                PostsFragment postsFragment = PostsFragment.newInstance("posts");
                Log.i(TAG, "getItem: posts " + Integer.toString(position));
                return postsFragment;
            case 1:
                LibraryFragment libraryFragment = LibraryFragment.newInstance();
                return libraryFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}

package com.kenyanewsfeed.app.tangaza.utils;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;

/**
 * Created by DI on 6/8/2016.
 */
public class RetrofitBuilder extends Activity {

    private static final String TAG = RetrofitBuilder.class.getSimpleName();

    private  String mUrl = "http://52.161.24.157/tangaza/";
    private  Retrofit mRetrofit;
    private  Context mContext;

    public RetrofitBuilder(Context context){
        if(context != null){
            mContext = context;
        } else {
            Log.i(TAG, "RetrofitBuilder: " + "Context is null");
        }

    }

    private File setUpCache(Context context){
        File httpCacheDirectory =
                new File(mContext.getCacheDir(),
                        "responses");

        return httpCacheDirectory;
    }



    public Retrofit getRetrofit(){
        if(mContext != null){
              OkHttpClient client = new OkHttpClient
                    .Builder()
                    .readTimeout(120, TimeUnit.SECONDS)
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .cache(new Cache(mContext.getCacheDir(), 10 * 1024 * 1024)) // 10 MB
                    .addInterceptor(new Interceptor() {
                        @Override public Response intercept(Chain chain) throws IOException {
                            Request request = chain.request();
                            if (CheckNetWork.isNetworkAvailableAndConnected(mContext)) {
                                request = request.newBuilder().header("Cache-Control", "public, max-age=" + 60).build();
                            } else {
                                request = request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + 60 * 60 * 24 * 7).build();
                            }
                            return chain.proceed(request);
                        }
                    })
                    .build();
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(mUrl)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
           // Toast.makeText(mContext, "Context in not null", Toast.LENGTH_SHORT).show();
        } else {
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(mUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return mRetrofit;
    }
}
